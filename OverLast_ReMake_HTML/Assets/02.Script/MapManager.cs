﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    public GameObject[] scoreBox;

    private void OnEnable()
    {
        for(int i =0;i<scoreBox.Length;i++)
        {
            if(scoreBox[i].activeSelf == false)
            {
                scoreBox[i].SetActive(true);
            }
        }
    }
}
