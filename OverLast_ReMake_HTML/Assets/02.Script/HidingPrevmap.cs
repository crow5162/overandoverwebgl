﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidingPrevmap : MonoBehaviour
{
    public bool isMapA;
    private const string PlayerTag = "Player";
    private bool hiddenObjShow = false;

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.tag == PlayerTag)
        {
            GameManager.instance.laps += 1;

            if(isMapA)
            {
                if(GameManager.instance.laps >= 2)
                {
                    hiddenObjShow = true;
                }

                GameManager.instance.MapBControll(false, hiddenObjShow);
            }
            else
            {
                if(GameManager.instance.laps >= 2)
                {
                    hiddenObjShow = true;
                }

                GameManager.instance.MapAController(false, hiddenObjShow);
            }

            this.gameObject.SetActive(false);
        }


    }
}
