﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleSceneChanger : MonoBehaviour
{
    public Text debugTextField = null;

    //첫 Title 화면에서 엔딩 유/무에따라 씬 바꾸어줌
    public void ChangeScene()
    {
        if(PlayerPrefs.HasKey("Ending"))
        {
            SceneManager.LoadScene(2);
        }
        else
        {
            SceneManager.LoadScene(1);
        }
    }

    private void Start()
    {
        //Screen.SetResolution(960, 600, false);
    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if (PlayerPrefs.HasKey("Ending"))
            {
                SceneManager.LoadScene(2);
            }
            else
            {
                SceneManager.LoadScene(1);
            }
        }
    }

    public void SaveDataClear()
    {
        //PlayerPrefs에 존재하는 모든 데이터 삭제
        PlayerPrefs.DeleteAll();
    }

    public void OnFullScreen()
    {
        //if(!Screen.fullScreen)
        //{
        //    Screen.SetResolution(960, 600, true);
        //}
        //else
        //{
        //    Screen.SetResolution(960, 600, false);
        //}
    }

    public void On3DSceneBtnDown()
    {
        SceneManager.LoadScene(2);
    }

}
