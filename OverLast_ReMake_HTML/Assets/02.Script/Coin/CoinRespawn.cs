﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinRespawn : MonoBehaviour
{
    public GameObject[] coin;

    void Start()
    {
        if(coin != null)
        {
            StartCoroutine(this.RespawnCoins());
        }
    }

    IEnumerator RespawnCoins()
    {
        //코인 전체를 검사하여 비활성화된 코인만을 다시 활성화
        for(int i =0;i<coin.Length;i++)
        {
            if(coin[i].activeSelf == false)
            {
                coin[i].SetActive(true);
            }
        }

        yield return new WaitForSeconds(30.0f);

        StartCoroutine(this.RespawnCoins());
    }
}
