﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject mapA, mapB;
    public GameObject hiddenObjectA, hiddenObjectB;
    public GameObject jumpBtn, actionBtn;
    public GameObject upBtnGuide;
    public int laps = 0;
    public Vector2 charaSpawnPoint;
    public GameObject mainCharacter;
    public Image keyImage;

    [SerializeField]
    private CharacterEmotionController emotion;
    [SerializeField]
    private GameObject[] coins;
    private bool isKey = false;
    public bool IsKey { get { return isKey; } }
    public bool actionBtnOn;

    [Header("Score Management")]
    [Range(0, 100)] public int initScore = 100;
    public int currentScore;

    public static GameManager instance = null;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        #region 2DSceneFunc

        //히든 요소 비활성화
        if(hiddenObjectA != null)
        {
            hiddenObjectA.SetActive(false);
        }

        //MapB를 비활성화 하기 전 코인을 캐싱
        coins = GameObject.FindGameObjectsWithTag("Coin");

        //코인을 캐싱후 MapB 비활성화
        if (mapB.activeSelf == true)
        {
            mapB.SetActive(false);
        }

        //게임 시작 시 현재 점수 초기화
        currentScore = initScore;

        //플레이어 감정 컨트롤러를 캐싱
        if(mainCharacter != null)
        {
            emotion = mainCharacter.GetComponent<CharacterEmotionController>();
        }
        else
        {
            Debug.Log("Main Character is NULL !! : GameManager");
        }

        if(keyImage != null)
        {
            if(keyImage.enabled)
            {
                keyImage.enabled = false;
            }
        }

        #endregion
    }

    #region 2DScene_Func

    public void MapAController(bool mapAControll, bool hiddenObj)
    {
        hiddenObjectA.SetActive(hiddenObj);
        mapA.SetActive(mapAControll);
    }

    public void MapBControll(bool mapBControll, bool hiddenObj)
    {
        hiddenObjectB.SetActive(hiddenObj);
        mapB.SetActive(mapBControll);
    }

    public void MapAMove()
    {
        Vector2 currMapApos = mapA.transform.position;
        currMapApos.x = 762 * laps;
        mapA.transform.position = currMapApos;
    }

    public void MapBMove()
    {
        Vector2 currMapBpos = mapB.transform.position;
        currMapBpos.x = 762 * laps;
        mapB.transform.position = currMapBpos;
    }

    public void CoinsRespawn()
    {
        //처음에 캐싱했던 코인을 기반으로 죽은 코인을 재생
        for(int i =0;i<coins.Length;i++)
        {
            if(coins[i].activeSelf == false)
            {
                coins[i].SetActive(true);
            }
        }
    }

    public void InitScoreFunc()
    {
        currentScore = initScore;
    }

    public void SpawnPointUpdate(Vector2 point)
    {
        charaSpawnPoint = point;
    }

    public void DecreaseScore(int num)
    {
        currentScore -= num;
    }

    public void IncreaseScore(int num)
    {
        currentScore += num;
    }

    public void CheckPlayerRank()
    {
        if (currentScore > 69)
        {
            emotion.playerRank = CharacterEmotionController.PlayerRank.RANK_A;
        }
        else if (currentScore <= 69 && currentScore > 49)
        {
            emotion.playerRank = CharacterEmotionController.PlayerRank.RANK_B;
        }
        else if (currentScore <= 49 && currentScore > 29)
        {
            emotion.playerRank = CharacterEmotionController.PlayerRank.RANK_C;
        }
        else
        {
            emotion.playerRank = CharacterEmotionController.PlayerRank.RANK_D;
        }
    }

    public void GetKey()
    {
        isKey = true;
        KeyImgUpdate();
    }

    public void UseKey()
    {
        isKey = false;
        KeyImgUpdate();
    }

    private void KeyImgUpdate()
    {
        if(isKey)
        {
            keyImage.enabled = true;
        }
        else
        {
            keyImage.enabled = false;
        }
    }

    public void OnActionBtnActive(bool isactive)
    {
        if (isactive)
        {
            jumpBtn.SetActive(false);
            actionBtn.SetActive(true);
        }
        else
        {
            actionBtn.SetActive(false);
            jumpBtn.SetActive(true);
        }
    }

    public void OnActionBtnDown()
    {
        actionBtnOn = true;
    }

    public void OnActionBtnUp()
    {
        actionBtnOn = false;
    }

    public void UpBtnGuideOn(bool imageOn)
    {
        if(imageOn)
        {
            upBtnGuide.SetActive(true);
        }
        else
        {
            upBtnGuide.SetActive(false);
        }
    }

    public void Go3DScene()
    {
        SceneManager.LoadScene(2);
    }

    #endregion

    #region 3DScene_Func

    #endregion

    #region UIOptions

    public void OnFullScreenBtnDown()
    {
        if(Screen.fullScreen)
        {
            Screen.SetResolution(960, 600, false);
        }
        else
        {
            Screen.SetResolution(960, 600, true);
        }
    }

    #endregion
}
