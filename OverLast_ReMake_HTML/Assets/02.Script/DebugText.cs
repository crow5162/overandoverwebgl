﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugText : MonoBehaviour
{
    public Text debugText = null;
    private int Width = 0;
    private int Height = 0;

    public void OnResize()
    {
        debugText.text = Width + "x" + Height;
        Screen.SetResolution(Width, Height, Screen.fullScreen);
    }

    public void SetWidth(int width)
    {
        Width = width;
    }

    public void SetHeight(int height)
    {
        Height = height;
    }

    public void OnFullScreen()
    {
        if(!Screen.fullScreen)
        {
            Screen.fullScreen = true;
        }
        else
        {
            Screen.fullScreen = false;
        }
    }
}
