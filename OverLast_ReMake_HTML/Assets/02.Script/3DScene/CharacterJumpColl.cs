﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterJumpColl : MonoBehaviour
{
    private const string playerTag = "Player";
    public float jumpPower = 10.0f;

    private void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == playerTag)
        {
            var playerRbody = coll.gameObject.GetComponent<Rigidbody>();
            playerRbody.AddForce(Vector3.up * jumpPower, ForceMode.Impulse);

            SoundManager.instance.StartSfx("JUMP");
        }
    }
}
