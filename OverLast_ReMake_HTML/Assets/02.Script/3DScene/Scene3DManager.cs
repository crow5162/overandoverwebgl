﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scene3DManager : MonoBehaviour
{
    [Header("3D Scene UI")]
    public CanvasGroup fadeInImg;
    public GameObject textField;
    public GameObject leftBtn, rightBtn;
    public GameObject jumpBtn;

    //public VideoPlayer video;
    public enum GameState
    {
        INTRO,
        INGAME
    }

    private bool isFadeIn = false;
    private float fades = 0.003f;
    public GameState gameState;

    public static Scene3DManager instance = null;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }

        //처음엔딩을 볼 때만 인트로를 실행하도록 한다
        if (!PlayerPrefs.HasKey("EndingIntroScene"))
        {
            PlayerPrefs.SetInt("EndingIntroScene", 1);
            gameState = GameState.INTRO;
            //gameState = GameState.INGAME;
        }
        else
        {
            //처음 엔딩씬의 인트로를 보고 두번째에 들어왔을 때에는 
            //인트로를 출력하지 않는다.
            textField.SetActive(false);
            OnEnabledButtons();
            gameState = GameState.INGAME;
        }
    }

    private void Start()
    {
        //엔딩씬에 처음 들어오면 다시 게임을 시작했을때, 다시 3D 씬으로 들어오기 위해 기록
        if(!PlayerPrefs.HasKey("Ending"))
        {
            PlayerPrefs.SetInt("Ending", 1);
        }
        
        //페이드 효과
        if(fadeInImg != null)
        {
            fadeInImg.alpha = 1.0f;
            isFadeIn = true;
        }

        if(gameState == GameState.INGAME)
        {
            SoundManager.instance.StartBackGroundMusic();
        }
    }

    private void Update()
    {
        #region FadeIn

        if(isFadeIn)
        {
            fadeInImg.alpha -= fades;

            if(fadeInImg.alpha <= 0)
            {
                isFadeIn = false;
                //fadeInImg.enabled = false;
            }
        }

        #endregion

        switch(gameState)
        {
            case GameState.INTRO:
                break;
            case GameState.INGAME:
                textField.SetActive(false);

                break;
        }
    }

    public void OnEnabledButtons()
    {
        leftBtn.SetActive(true);
        rightBtn.SetActive(true);
        jumpBtn.SetActive(true);
    }

    public void DeleteAllKeys()
    {
        PlayerPrefs.DeleteAll();
    }

}
