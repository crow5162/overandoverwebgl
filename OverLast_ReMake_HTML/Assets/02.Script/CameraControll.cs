﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControll : MonoBehaviour
{
    public Transform playerTr;

    private CharacterStatus playerStatus;
    private bool manageMode = false;
    private float cameraSpeed = 15.0f;
    private AudioSource _audio;

    private void Start()
    {
        if(playerTr != null)
        {
            playerStatus = playerTr.gameObject.GetComponent<CharacterStatus>();

            if(playerStatus == null)
            {
                Debug.Log("캐릭터 스테이터스가 NULL 입니다 ! : CameraControll");
            }
        }
        else
        {
            Debug.Log("캐릭터 게임 오브젝트가 NULL 입니다 ! : CameraControll");

            manageMode = true;
        }

        _audio = GetComponent<AudioSource>();
        _audio.Play();
        _audio.loop = true;
    }

    private void Update()
    {
        #region CameraMoveing
        if(manageMode)
        {
            if(Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Translate(Vector2.left * cameraSpeed * Time.deltaTime);
            }
            if(Input.GetKey(KeyCode.RightArrow))
            {
                transform.Translate(Vector2.right * cameraSpeed * Time.deltaTime);
            }
            if(Input.GetKey(KeyCode.UpArrow))
            {
                transform.Translate(Vector2.up * cameraSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                transform.Translate(Vector2.down * cameraSpeed * Time.deltaTime);
            }
        }
        #endregion  
    }

    private void LateUpdate()
    {
        if (playerTr == null)
            return;

        if(!playerStatus.IsDie)
        {
            transform.position = new Vector3(playerTr.position.x, playerTr.position.y, transform.position.z);
        }
    }
}
