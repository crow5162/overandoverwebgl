﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct GameSfx
{
    public AudioClip jumpSfx;
    public AudioClip deathSfx;
    public AudioClip getCoin;
    public AudioClip enterPortal;
}

public class SoundManager : MonoBehaviour
{
    private AudioSource _audio;

    public static SoundManager instance = null;
    public GameSfx gameSfx;

    [Range(0.0f, 1.0f)] public float audioVolume = 1.0f;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        _audio = GetComponent<AudioSource>();

    }

    public void StartBackGroundMusic()
    {
        _audio.Play();
        _audio.loop = true;
    }

    /// <summary>
    /// 효과음을 재생하는 함수 입니다 : "SoundManager"
    /// </summary>
    /// <param name="sfxName">어떤 효과음을 재생할 지 설정합니다 : JUMP, COIN, DEATH, PORTAL</param>
    public void StartSfx(string sfxName)
    {
        if(sfxName == "JUMP")
        {
            var jumpsfx = gameSfx.jumpSfx;
            _audio.PlayOneShot(jumpsfx, audioVolume);
        }
        else if (sfxName == "COIN")
        {
            var getcoinsfx = gameSfx.getCoin;
            _audio.PlayOneShot(getcoinsfx, audioVolume);
        }
        else if(sfxName == "DEATH")
        {
            var deathsfx = gameSfx.deathSfx;
            _audio.PlayOneShot(deathsfx, audioVolume);
        }
        else if (sfxName == "PORTAL")
        {
            var portalsfx = gameSfx.enterPortal;
            _audio.PlayOneShot(portalsfx, audioVolume);
        }
    }


}
