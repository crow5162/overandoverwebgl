﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMoving : MonoBehaviour
{
    public GameObject mapA;

    //private void OnCollisionEnter2D(Collision2D coll)
    //{
    //    if(coll.gameObject.tag == "Player")
    //    {
    //        Vector2 mapApos = mapA.transform.position;
    //        mapApos.x += 20.0f;
    //        mapA.transform.position = mapApos;
    //
    //        Debug.Log("On Collision !");
    //    }
    //}

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            Vector2 mapApos = mapA.transform.position;
            mapApos.x += 20.0f;
            mapA.transform.position = mapApos;

            Debug.Log("On Collision !");
        }
    }
}
