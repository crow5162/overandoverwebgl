﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnOffText : MonoBehaviour
{
    public Text blinkText;
    public float blinkTime = 1.0f;
    private float blinkTimeSet;

    private void Update()
    {
        blinkTimeSet += Time.deltaTime;

        //Title Scene의 Image 깜빡임 
        if(blinkTimeSet < blinkTime)
        {
            blinkText.enabled = true;
        }
        else if(blinkTimeSet >= blinkTime)
        {
            blinkText.enabled = false;
        }

        if(blinkTimeSet >= blinkTime * 2)
        {
            blinkTimeSet = 0.0f;
        }
    }

}
