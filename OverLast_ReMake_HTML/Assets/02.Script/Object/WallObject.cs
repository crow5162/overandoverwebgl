﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallObject : MonoBehaviour
{
    private bool isOpen;
    private float openTime;

    private void Update()
    {
        if(isOpen)
        {
            openTime += Time.deltaTime;

            if(openTime < 0.5f)
            {
                transform.Translate(Vector2.up * 10.0f * Time.deltaTime);
            }
            else
            {
                this.gameObject.SetActive(false);
            }
        }
        
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("Player"))
        {
            if (GameManager.instance.IsKey)
            {
                GameManager.instance.UseKey();
                isOpen = true;
            }
        }
    }
}
