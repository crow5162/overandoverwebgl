﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpScoreBox : MonoBehaviour
{
    public int decreaseScoreCount = 3;

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.CompareTag("Player"))
        {
            this.gameObject.SetActive(false);
            GameManager.instance.DecreaseScore(decreaseScoreCount);
            GameManager.instance.CheckPlayerRank();
        }
    }
}
