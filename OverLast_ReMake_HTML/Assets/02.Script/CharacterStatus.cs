﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CharacterStatus : MonoBehaviour
{
    private bool isDie;
    private bool stopMotionRankB;
    private Rigidbody2D rBody;
    private BoxCollider2D boxColl;
    private SpriteRenderer sprite;
    private CharacterControll charaControll;
    private CharacterEmotionController emotion;
    private int deathCount = 0;

    [SerializeField]
    private Vector2 respawnPoint;
    public float dieForce = 10.0f;
    public float knockBackForce = 0.05f;
    public UnityEngine.UI.Text deathCountText;

    public delegate void CharaInitHandler();
    public static event CharaInitHandler OnInitChara;
    CharaInitHandler charaInit;

    public bool IsDie { get { return isDie; } set { isDie = value; } }
    public bool StopMotionRankB { get { return stopMotionRankB; } set { stopMotionRankB = value; } }

    private void Start()
    {
        rBody = GetComponent<Rigidbody2D>();
        boxColl = GetComponent<BoxCollider2D>();
        charaControll = gameObject.GetComponent<CharacterControll>();
        sprite = GetComponent<SpriteRenderer>();
        emotion = GetComponent<CharacterEmotionController>();

        //respawnPoint = GameManager.instance.RespawnPoint.position;

        isDie = false;
    }

    IEnumerator PlayerDieFunc()
    {
        respawnPoint = GameManager.instance.charaSpawnPoint;
        boxColl.isTrigger = true;
        isDie = true;
        UpdateDeathCount();

        yield return new WaitForSeconds(1.5f);

        //사망 시 점수는 초기화
        GameManager.instance.InitScoreFunc();
        GameManager.instance.CheckPlayerRank();
        emotion.IsAngry = false;
        OnInitChara();
        boxColl.isTrigger = false;
        isDie = false;
        emotion.IsResign = false;
        emotion.FindTime = 0.0f;
        transform.position = respawnPoint;

        if(stopMotionRankB)
        {
            stopMotionRankB = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        //Spike & Water Collision 
        if(coll.gameObject.layer == LayerMask.NameToLayer("Spike"))
        {
            rBody.AddForce(Vector2.up * dieForce, ForceMode2D.Impulse);
            charaControll.CharacterDieAni();
            StartCoroutine(this.PlayerDieFunc());
            charaControll.DeadSound();
        }
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.layer == LayerMask.NameToLayer("Coin"))
        {
            coll.gameObject.SetActive(false);
            GameManager.instance.IncreaseScore(1);
            GameManager.instance.CheckPlayerRank();
            charaControll.CoinSound();
        }

        if(coll.gameObject.layer == LayerMask.NameToLayer("GoldenKey"))
        {
            //사운드 효과만 출력해줌
            charaControll.GetKeySound();
        }
    }

    //Collision Enemy Direction = (0 == LEFT, 0 != RIGHT)
    public void CollisionWithEnemy(int direction)
    {
        if(direction == 0)
        {
            //RigidBody를 이용하여 밀어내는 방식으로 만들지 않은거같아 Translate를 사용하여 일부로
            //끊기는 듯한 느낌을 주었음.
            //rBody.AddForce(Vector2.left * knockBackForce, ForceMode2D.Impulse);
            transform.Translate(Vector2.left * knockBackForce);
        }
        else
        {
            //rBody.AddForce(Vector2.right * knockBackForce, ForceMode2D.Impulse);
            transform.Translate(Vector2.right * knockBackForce);
        }
    }

    private void UpdateDeathCount()
    {
        deathCount += 1;
        deathCountText.text = deathCount.ToString();
    }
}
